package persistencia;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import dto.Persona;
import grafo.Grafo;
import grafo.GrafoDirigido;
import serializer.Serializer;

public class Serializador implements Serializer{

	private Grafo grafo;
	private GrafoDirigido gd;
	private List<Persona> empleados;
	
	public Serializador(Grafo grafo, GrafoDirigido gd) {
		this.grafo = grafo;
		this.gd = gd;
		
	}

	@Override
	public void resolver() {
		serializar(0);
	}

	
	public void serializar(int vertice)  {
		if(vertice  == grafo.tamano()){
				System.out.println("serializacion ok");

			//Caso base
		}else{
			serializarGrafo(vertice);
			serializar(vertice + 1);
		}
	}

		
	public void serializarGrafo(int vertice) {
		File file = new File("jerarquia.txt");
		ArrayList<Persona> grafo = new ArrayList<Persona>();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
			for(Persona e: gd.getEmpleados()) {
				if(e.getId() == vertice) {
					grafo.add(e);
				}
			}
					oos.writeObject(grafo);
					oos.close();

		}catch(Exception e) {
			e.printStackTrace();
		}	
	}

	@Override
	public List<Persona> deserializar() throws ClassNotFoundException {
		File file = new File("jerarquia.txt");
		
	    List<Persona> recordList = new ArrayList<Persona>();

		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
			Persona p1 = (Persona) ois.readObject();
			ois.close();
		}catch(IOException e){
			e.printStackTrace();
		}
		return recordList;
	}
}
	
